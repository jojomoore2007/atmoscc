# AtmosCC

An abstraction layer to safeguard your computer, shielding your data, and notifying you whenever something tries to harm your computer.

### Features:

- Keeps everything in an airtight seal, so you don't have to worry about anything harming your system.
- Saves a backup of the last boot, so you can roll back if things ever get broken.
- Starts on boot without any need for user interaction.
- Keeps unauthorized startup disks from running.

### Install:

Just type in `pastebin run XXXXX` and let it do the rest. With multiple layers of abstraction, you can now sit worry-free, knowing your computer is safe.