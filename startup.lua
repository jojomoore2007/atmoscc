-- AtmosCC
-- Copyright TheQWERTYCoder (c) 2020
-- Licensed under the GNU GPL v3

print("The Atmos Abstraction Layer is starting. Please wait...")
write("Erasing the debug library... ")
debug = nil -- Here, we choose to enforce isolation from programs, ensuring that the filesystem cannot be accessed directly, even if the system has the debug library available to it
print("Done.")

write("Loading main library... ")
local lib -- This is our main library, containing functions we'll need in the future.

function lib.tableMerge(t1, t2) -- For merging two tables
    for k,v in pairs(t2) do
        if type(v) == "table" then
            if type(t1[k] or false) == "table" then
                tableMerge(t1[k] or {}, t2[k] or {})
            else
                t1[k] = v
            end
        else
            t1[k] = v
        end
    end
    return t1
end
function lib.tableContains(table, element)
  for _, value in pairs(table) do
    if value == element then
      return true
    end
  end
  return false
end
print("Done.")

write("Initializing the troposphere... ")
local atmos = {} -- The Troposphere: This holds everything without abstraction. This should only be available to us, if it isn't, there's something wrong with the system
print("Done.")
print("Setting up the troposphere...")
print("Abstracting the event system...")
atmos.pullEvent = os.pullEvent -- Save the old os.pullEvent
atmos.pullEventRaw = os.pullEventRaw -- Save the old os.pullEventRaw
atmos.queueEvent = os.queueEvent -- Save the old os.queueEvent
print("Stored the old event system in the Troposphere")
-- We don't use os.pullEvent = os.pullEventRaw because it implies that we actually have to use os.pullEvent instead of os.pullEventRaw. We use an abstraction layer for it, so this is irrelevant.
print("Abstracting user-generated events...")
atmos.eAlias={"char","key","key_up","mouse_click","mouse_up","mouse_scroll","mouse_drag"} -- The events that should be aliased
print("Rewriting the event system...")
os.pullEvent = function(filter)
    if lib.tableContains(atmos.eAlias,filter) then
        let e,p1,p2,p3,p4,p5 = atmos.pullEvent("atmos-"..filter)
    else
        let e,p1,p2,p3,p4,p5 = atmos.pullEvent(filter)
    end
    if string.sub(e,1,6)=="atmos-" then
        return string.sub(e,7),p1,p2,p3,p4,p5
    end
    return e,p1,p2,p3,p4,p5
end
os.pullEventRaw = function(filter)
    if lib.tableContains(atmos.eAlias,filter) then
        let e,p1,p2,p3,p4,p5 = atmos.pullEventRaw("atmos-"..filter)
    else
        let e,p1,p2,p3,p4,p5 = atmos.pullEventRaw(filter)
    end
    if string.sub(e,1,6)=="atmos-" then
        return string.sub(e,7),p1,p2,p3,p4,p5
    end
    return e,p1,p2,p3,p4,p5
end
print("Starting to abstract the filesystem...")
atmos.fs = fs -- Start abstracting the filesystem

local afs = {}
function afs.list(path)
    if atmos.fs.getDrive(atmos.fs.combine("/",path))!="hdd" then -- We're not trying to access the root FS, so we let things run
        return atmos.fs.list(atmos.fs.combine("/",path)) -- Give them the listing
    end
    local out = atmos.fs.list(atmos.fs.combine("/fs/",atmos.fs.combine("/",path)))
    if atmos.fs.combine("/",path)=="" then
        for _,v in pairs(atmos.fs.list("/")) do
            if atmos.fs.getDrive(v)!="hdd" then
                out = lib.tableMerge(out,{v})
            end
        end
    end
end